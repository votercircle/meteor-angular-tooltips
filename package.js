Package.describe({
  name: 'votercircle:meteor-angular-tooltips',
  summary: 'Angular tooltips packaged for use with Meteor apps',
  version: '1.0.0',
  git: 'git@bitbucket.org:votercircle/meteor-angular-tooltips.git'
});

Package.onUse(function(api) {
  api.versionsFrom('1.0');
  api.use('urigo:angular@0.8.8', 'client');
  api.addFiles('angular-tooltips.js', 'client');
  api.addFiles('angular-tooltips.css', 'client');
});
